set fish_greeting # Supresses fish's intro message
set TERM "xterm-256color" # Sets the terminal type



if status is-interactive
    # Commands to run in interactive sessions can go here

    set fish_color_normal brcyan
    set fish_color_autosuggestion '#7d7d7d'
    set fish_color_command brcyan
    set fish_color_error '#ff6c6b'
    set fish_color_param brcyan


    seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo
    pfetch | lolcat

    starship init fish | source
end
